import os
import glob
import subprocess
import streamlit as st
import papermill as pm


TEMPLATES_PATH = "templates"
RESULTS_PATH = "temp/reports"


if __name__ == "__main__":

    st.write("Reports executor")
    template_path = st.selectbox("select template", glob.glob(os.path.join(TEMPLATES_PATH, "*.ipynb")))
    result_path = st.text_input("select result path", os.path.join(RESULTS_PATH, "report.ipynb"))
    default_params = {k: v["default"] for k, v in pm.inspect_notebook(template_path).items()}
    st.write(default_params)
    params = {}
    for k, v in default_params.items():
        params[k] = st.text_input(k, v)
    add_input = st.selectbox("Add input cells while nbconvert", ["yes", "no"])

    if st.button("Execute"):
        with st.spinner("Please wait..."):
            pm.execute_notebook(
                template_path,
                result_path,
                parameters=params
            )

            cmd_command = ["jupyter", "nbconvert", "--to", "html", result_path]

            if add_input == "no":
                cmd_command.extend(["--no-input"])
            subprocess.run(cmd_command)
