# reports

Parametrized reports with dashboard-executor.

## Installation
```shell
# main dependencies
pip install -r requirements/main.txt

# dev dependencies
pip install -r requirements/dev.txt
```